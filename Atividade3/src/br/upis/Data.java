package br.upis;

public class Data implements IData {
	private byte dia;
	private byte mes;
	private short ano;
	
	private boolean ehBissexto (short ano) {
		return (ano % 400 == 0) || ((ano % 4 == 0) && (ano % 100 != 0));
	}
	private byte getUltimoDia (byte mes, short ano) {
		byte ud [] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
		if (mes == 2 && ehBissexto (ano)) {
			return 29;
		}
		return ud [mes];
	}
	public Data () {
		setAno ((byte)1);
		setMes ((byte)1);
		setDia ((byte)1);
	}
	public Data (byte dia, byte mes, short ano) {
		this ();
		setAno (ano);
		setMes (mes);
		setDia (dia);
	}
	public Data (int dia, int mes, int ano) {
		this ((byte)dia, (byte)mes, (short)ano);
	}
	@Override
	public byte getDia () {
		return dia;
	}
	@Override
	public void setDia (byte dia) {
		byte ultimoDia = getUltimoDia (mes, ano);
		if (dia >= 1 && dia <= ultimoDia) {
			this.dia = dia;
		}	
	}
	@Override
	public byte getMes() {
		return mes;
	}
	@Override
	public void setMes (byte mes) {
		if (mes >= 1 && mes <= 12) {
			this.mes = mes;
		}
	}
	@Override
	public short getAno() {
		return ano;
	}
	@Override
	public void setAno (short ano) {
		if (ano >= 1 && ano <= 9999) {
			this.ano = ano;
		}
	}
	@Override
	public String toString () {
		return " Data - " + getDia() + " / " + getMes() + " / " + getAno();
	}
	@Override
	public void incrementaAno () {
		short year = (short)(ano + 1);
		if (year == 9999) {
			ano = 0;
		}else {
			ano = year;
		}
	}
	@Override
	public void incrementaAnonvezes (int n) {
		int i = 0;
		
		while (n > 0 && i < n) {
			this.incrementaAno();
			i++;
		}
	}
	@Override
	public void incrementaMes () {
		byte month = (byte)(mes + 1);
		if (month == 12) {
			mes = 1;
			incrementaAno();
		}else {
			mes = month;
		}
	}
	@Override
	public void incrementaMesnvezes (int n) {
		int i = 0;
		
		while (n > 0 && i < n) {
			this.incrementaMes();
			i++;
		}
	}
	@Override
	public void incrementaDia () {
		byte day = (byte)(dia + 1);
		if (day == getUltimoDia (mes, ano)) {
			dia = 1;
			incrementaMes();
		}else {
			dia = day;
		}
	}
	@Override
	public void incrementaDianvezes (int n) {
		int i = 0;
		
		while (n > 0 && i < n) {
			this.incrementaDia();
			i++;
		}
	}
	private boolean equalsAno (Data d) {
		return this.getAno() == d.getAno();
	}
	private boolean equalsMes (Data d) {
		return this.getMes() == d.getMes();
	}
	private boolean equalsDia (Data d) {
		return this.getDia() == d.getDia();
	}
	public boolean equals (Object obj) {
		
		if(this == obj)
			return true;
		
		if(obj == null || obj.getClass()!= this.getClass())
			return false;
		
		Data d = (Data) obj;
		
		return equalsAno (d) && equalsMes (d) && equalsDia (d);
	}
	@Override
	public int hashCode () {
		final int prime = 31;
		int result = 1;
		result = prime * result + ano;
		result = prime * result + dia;
		result = prime * result + mes;
		return result;
	}
	public boolean verificaMenor (Data d) {
		
		if (this.getAno() < d.getAno()) {
			return true;
		}
		if (equalsAno (d) && this.getMes() < d.getMes()) {
			return true;
	    }
		if (equalsAno (d) && equalsMes (d) && this.getDia() < d.getDia()) {
			return true;
		}
		return false;
	}
	public boolean verificaMaior (Data d) {
		
		if (this.getAno() > d.getAno()) {
			return true;
		}
		if (equalsAno (d) && this.getMes() > d.getMes()) {
			return true;
	    }
		if (equalsAno (d) && equalsMes (d) && this.getDia() > d.getDia()) {
			return true;
		}
		return false;
	}
	public boolean verificaLe (Data d) {
		
		return this.equals (d) || this.verificaMenor(d);
	}
	public boolean verificaGe (Data d) {
		
		return this.equals (d) || this.verificaMaior(d);
	}
}
