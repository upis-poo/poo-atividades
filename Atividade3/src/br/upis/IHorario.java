package br.upis;

public interface IHorario {

	void setHora(byte hora);

	int getHora();

	void setMinuto(byte minuto);

	int getMinuto();

	void setSegundo(byte segundo);

	int getSegundo();

	String toString();

	void incrementaSegundo();

	void incrementaSegundonvezes(int n);

	void incrementaMinuto();

	void incrementaMinutonvezes(int n);

	void incrementaHora();

	void incrementaHoranvezes(int n);

	boolean ehUltimoHorario();

	boolean ehPrimeiroHorario();

}