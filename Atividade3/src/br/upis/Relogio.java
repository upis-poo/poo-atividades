package br.upis;

public class Relogio {
	private IHorario hms;
	private IData dma;
	
	public Relogio(IHorario hms, IData dma) {
		this.hms = new Horario(hms);
		this.dma = dma;
	}
	
	public void tictac() {
		hms.incrementaSegundo();
		if(hms.ehPrimeiroHorario()) {
			dma.incrementaDia();
		}		
	}
	public String toString() {
		return dma + " " + hms;
	}
	private boolean equalsAno (Relogio rlg) {
		return this.dma.getAno() == rlg.dma.getAno();
	}
	private boolean equalsMes (Relogio rlg) {
		return this.dma.getMes() == rlg.dma.getMes();
	}
	private boolean equalsDia (Relogio rlg) {
		return this.dma.getDia() == rlg.dma.getDia();
	}
	private boolean equalsHora (Relogio rlg) {
		return this.hms.getHora() == rlg.hms.getHora();
	}
	private boolean equalsMinuto (Relogio rlg) {
		return this.hms.getMinuto() == rlg.hms.getMinuto();
	}
	private boolean equalsSegundo (Relogio rlg) {
		return this.hms.getSegundo() == rlg.hms.getSegundo();
	}
	public boolean equals (Object obj) {
		
		if(this == obj)
			return true;
		
		if(obj == null || obj.getClass()!= this.getClass())
			return false;
		
		Relogio rlg = (Relogio) obj;
		
		return equalsAno (rlg) &&
			   equalsMes (rlg) &&
			   equalsDia (rlg) &&
			   equalsHora (rlg) &&
			   equalsMinuto (rlg) &&
			   equalsSegundo (rlg);
	}
	public boolean verificaMenor (Relogio rlg) {
		
		if (this.dma.getAno() < rlg.dma.getAno()) {
			return true;
		}
		if (equalsAno (rlg) && this.dma.getMes() < rlg.dma.getMes()) {
			return true;
	    }
		if (equalsAno (rlg) && equalsMes (rlg) && this.dma.getDia() < rlg.dma.getDia()) {
			return true;
		}
		if (equalsAno (rlg) && equalsMes (rlg) && equalsDia (rlg) && this.hms.getHora() < rlg.hms.getHora()) {
			return true;
		}
		if (equalsAno (rlg) && equalsMes (rlg) && equalsDia (rlg) && equalsHora (rlg) && this.hms.getMinuto() < rlg.hms.getMinuto()) {
			return true;
	    }
		if (equalsAno (rlg) && equalsMes (rlg) && equalsDia (rlg) && equalsHora (rlg) && equalsMinuto (rlg) && this.hms.getSegundo() < rlg.hms.getSegundo()) {
			return true;
		}
		return false;
	}
	public boolean verificaMaior (Relogio rlg) {
		
		if (this.dma.getAno() > rlg.dma.getAno()) {
			return true;
		}
		if (equalsAno (rlg) && this.dma.getMes() > rlg.dma.getMes()) {
			return true;
	    }
		if (equalsAno (rlg) && equalsMes (rlg) && this.dma.getDia() > rlg.dma.getDia()) {
			return true;
		}
		if (equalsAno (rlg) && equalsMes (rlg) && equalsDia (rlg) && this.hms.getHora() > rlg.hms.getHora()) {
			return true;
		}
		if (equalsAno (rlg) && equalsMes (rlg) && equalsDia (rlg) && equalsHora (rlg) && this.hms.getMinuto() > rlg.hms.getMinuto()) {
			return true;
	    }
		if (equalsAno (rlg) && equalsMes (rlg) && equalsDia (rlg) && equalsHora (rlg) && equalsMinuto (rlg) && this.hms.getSegundo() > rlg.hms.getSegundo()) {
			return true;
		}
		return false;
	}
	public boolean verificaLe (Relogio rlg) {
		
		return this.equals (rlg) || this.verificaMenor(rlg);
	}
	public boolean verificaGe (Relogio rlg) {
		
		return this.equals (rlg) || this.verificaMaior(rlg);
	}
}
