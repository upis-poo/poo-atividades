package br.upis;

public class HorarioNG implements IHorario{
	private int segundo;
	
	public HorarioNG () {
		setSegundo ((byte)0);
	}
	public HorarioNG (byte hora, byte minuto, byte segundo) {
		setHora (hora);
		setMinuto (minuto);
		setSegundo (segundo);
	}
	@Override
	public void setHora(byte hora) {
		if (hora >= 0 && hora <= 23) {
			this.segundo = segundo + hora * 3600;
		}
	}

	@Override
	public int getHora() {
		int hora;
		hora = (segundo/3600);
		return hora;
	}

	@Override
	public void setMinuto(byte minuto) {
		if (minuto >= 0 && minuto <= 59) {
			this.segundo = segundo + minuto * 60;
		}
	}

	@Override
	public int getMinuto() {
		int hora;
		int minuto;
		hora = (segundo/3600);
		minuto = ((segundo - hora * 3600)/60);
		return minuto;
	}

	@Override
	public void setSegundo(byte s) {
		if (s >= 0 && s <= 59) {
			this.segundo = segundo + s;
		}
	}

	@Override
	public int getSegundo() {
		int hora;
		int m;
		int s;
		hora = (segundo/3600);
		m = ((segundo - hora * 3600)/60);
		s = (segundo - hora * 3600 - m * 60);
		return s;
	}

	@Override
	public void incrementaSegundo() {
		if (segundo < 86399) {
			segundo = segundo + 1;
		}
	}

	@Override
	public void incrementaSegundonvezes(int n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void incrementaMinuto() {
		if (segundo / 60 < 1440) {
			segundo = segundo + 60;
		}
	}

	@Override
	public void incrementaMinutonvezes(int n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void incrementaHora() {
		if (segundo / 3600 < 24) {
			segundo = segundo + 3600;
			}
	}

	@Override
	public void incrementaHoranvezes(int n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean ehUltimoHorario() {
		return segundo == 86399;
	}

	@Override
	public boolean ehPrimeiroHorario() {
		return segundo == 0;
	}
	private boolean equalsHora (HorarioNG h) {
		return this.getHora() == h.getHora();
	}
	private boolean equalsMinuto (HorarioNG h) {
		return this.getMinuto() == h.getMinuto();
	}
	private boolean equalsSegundo (HorarioNG h) {
		return this.getSegundo() == h.getSegundo();
	}
	public boolean equals (Object obj) {
		
		if(this == obj)
			return true;
		
		if(obj == null || obj.getClass()!= this.getClass())
			return false;
		
		HorarioNG h = (HorarioNG) obj;
		
		return equalsHora (h) && equalsMinuto (h) && equalsSegundo (h);
	}
	@Override
	public int hashCode () {
		final int prime = 31;
		int result = 1;
		result = prime * result + segundo;
		return result;
	}
	public boolean verificaMenor (HorarioNG h) {
		
		if (this.getHora() < h.getHora()) {
			return true;
		}
		if (equalsHora (h) && this.getMinuto() < h.getMinuto()) {
			return true;
	    }
		if (equalsHora (h) && equalsMinuto (h) && this.getSegundo() < h.getSegundo()) {
			return true;
		}
		return false;
	}
	public boolean verificaMaior (HorarioNG h) {
		
		if (this.getHora() > h.getHora()) {
			return true;
		}
		if (equalsHora (h) && this.getMinuto() > h.getMinuto()) {
			return true;
	    }
		if (equalsHora (h) && equalsMinuto (h) && this.getSegundo() > h.getSegundo()) {
			return true;
		}
		return false;
	}
	public boolean verificaLe (HorarioNG h) {
		
		return this.equals (h) || this.verificaMenor(h);
	}
	public boolean verificaGe (HorarioNG h) {
		
		return this.equals (h) || this.verificaMaior(h);
	}
}
