package br.upis;

public class DataNG implements IData{
	private int dia;
	
	private boolean ehBissexto (int ano) {
		return (ano % 400 == 0) || ((ano % 4 == 0) && (ano % 100 != 0));
	}
	private byte getUltimoDia (byte mes, short ano) {
		byte ud [] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
		if (mes == 2 && ehBissexto (ano)) {
			return 29;
		}
		return ud [mes];
	}
	public DataNG () {
		setDia ((byte)1);
	}
	public DataNG (byte dia, byte mes, short ano) {
		this ();
		setAno (ano);
		setMes (mes);
		setDia (dia);
	}
	public DataNG (int dia, int mes, int ano) {
		this ((byte)dia, (byte)mes, (short)ano);
	}

	@Override
	public byte getDia() {
		int sexto, ano, bi6;
		
		sexto = (getAno() - 1)/4;
		ano = dia - (365 * (getAno()) + sexto);
		bi6 = 0;
		
		if (ehBissexto (getAno())) {
			bi6 = 1;
		}
		
		int diaMes [] = {0, 0, 31, 59 + bi6, 90 + bi6, 120 + bi6, 151 + bi6, 181 + bi6, 212 + bi6, 243 + bi6, 273 + bi6, 304 + bi6, 334 + bi6};
		int diia = ano - diaMes[getMes()];
		return (byte) diia;
	}

	@Override
	public void setDia(byte diia) {
		byte ultimoDia = getUltimoDia (getMes(), getAno());
		if (diia >= 1 && diia <= ultimoDia) {
			dia = dia - getDia() + diia;
		}
	}

	@Override
	public byte getMes() {
		int sexto, ano, mes, verifica, sextou;

		sexto = (getAno() - 1)/4;
		ano = dia - (365 * (getAno()) + sexto);
		mes = 1;
		verifica = getAno();
		sextou = 0;
		
		if (ehBissexto (verifica)) {
			sextou = 1;
		}
		if ((ano >= 1) && (ano < 32)) {
			mes = 1;
		}
		if ((ano > 31 + sextou) && (ano < 61 + sextou)) {
			mes = 2;
		}
		if ((ano > 60 + sextou) && (ano < 92 + sextou)) {
			mes = 3;
		}
		if ((ano > 91 + sextou) && (ano < 122 + sextou)) {
			mes = 4;
		}
		if ((ano > 121 + sextou) && (ano < 153 + sextou)) {
			mes = 5;
		}
		if ((ano > 152 + sextou) && (ano < 183 + sextou)) {
			mes = 6;
		}
		if ((ano > 182 + sextou) && (ano < 214 + sextou)) {
			mes = 7;
		}
		if ((ano > 213 + sextou) && (ano < 245 + sextou)) {
			mes = 8;
		}
		if ((ano > 244 + sextou) && (ano < 275 + sextou)) {
			mes = 9;
		}
		if ((ano > 274 + sextou) && (ano < 306 + sextou)) {
			mes = 10;
		}
		if ((ano > 305 + sextou) && (ano < 336 + sextou)) {
			mes = 11;
		}
		if ((ano > 335 + sextou) && (ano < 366 + sextou)) {
			mes = 12;
		}
		return(byte) mes;
	}

	@Override
	public void setMes(byte mes) {
		if (mes >= 1 && mes <= 12) {
			int sextou = 0;
			if (ehBissexto (getAno())) {
				sextou = 1;
			}
			int diaMes [] = {0, 0, 31, 59 + sextou, 90 + sextou, 120 + sextou, 151 + sextou, 181 + sextou, 212 + sextou, 243 + sextou, 273 + sextou, 304 + sextou, 334 + sextou};
			int diferensa = diaMes[mes] - diaMes[getMes()];			
			dia = dia + diferensa;
		}
	}

	@Override
	public short getAno() {
		int ano;
		ano = (dia/365);
		return(short) ano;
	}

	@Override
	public void setAno(short ano) {
		if (ano >= 1 && ano <= 9999) {
			int sexto = (getAno () - 1)/4;
			int anno = 365 * ((getAno () - 1) + sexto);
			int diferensa = dia - anno;
			int sextoverifica = (ano - 1)/4;
			dia = diferensa + (365 * ano) + sextoverifica;
		}
	}

	@Override
	public void incrementaAno() {
		int anoSeguinte, mes;
		
		anoSeguinte = getAno () + 1;
		mes = getMes ();
		
		if ((anoSeguinte - 1) > 0 && (anoSeguinte-1) < 9999) {
			if ((ehBissexto (anoSeguinte) && mes > 2) || (ehBissexto (anoSeguinte -1) && mes < 3)) {
				dia = dia + 366;
			}
			else {
				dia = dia + 365;
			}
		}
	}

	@Override
	public void incrementaAnonvezes(int n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void incrementaMes() {
		int ultimo;
		
		ultimo = getUltimoDia (getMes (), getAno ());
		dia = dia + ultimo;
		}

	@Override
	public void incrementaMesnvezes(int n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void incrementaDia() {
		dia =  dia + 1;
	}

	@Override
	public void incrementaDianvezes(int n) {
		// TODO Auto-generated method stub
		
	}
	private boolean equalsAno (DataNG dt) {
		return this.getAno() == dt.getAno();
	}
	private boolean equalsMes (DataNG dt) {
		return this.getMes() == dt.getMes();
	}
	private boolean equalsDia (DataNG dt) {
		return this.getDia() == dt.getDia();
	}
	public boolean equals (Object obj) {
		
		if(this == obj)
			return true;
		
		if(obj == null || obj.getClass()!= this.getClass())
			return false;
		
		DataNG dt = (DataNG) obj;
		
		return equalsAno (dt) && equalsMes (dt) && equalsDia (dt);
	}
	@Override
	public int hashCode () {
		final int prime = 31;
		int result = 1;
		result = prime * result + dia;
		return result;
	}
	public boolean verificaMenor (DataNG dt) {
		
		if (this.getAno() < dt.getAno()) {
			return true;
		}
		if (equalsAno (dt) && this.getMes() < dt.getMes()) {
			return true;
	    }
		if (equalsAno (dt) && equalsMes (dt) && this.getDia() < dt.getDia()) {
			return true;
		}
		return false;
	}
	public boolean verificaMaior (DataNG dt) {
		
		if (this.getAno() > dt.getAno()) {
			return true;
		}
		if (equalsAno (dt) && this.getMes() > dt.getMes()) {
			return true;
	    }
		if (equalsAno (dt) && equalsMes (dt) && this.getDia() > dt.getDia()) {
			return true;
		}
		return false;
	}
	public boolean verificaLe (DataNG dt) {
		
		return this.equals (dt) || this.verificaMenor(dt);
	}
	public boolean verificaGe (DataNG dt) {
		
		return this.equals (dt) || this.verificaMaior(dt);
	}
}
