package br.upis.collection;

import java.util.Comparator;

import br.upis.DataNG;

public class SortDataNG implements Comparator < DataNG > {
	
	@Override
	public int compare (DataNG o1, DataNG o2) {
		if (o1.verificaMenor(o2)) return -1;
		if (o1.equals(o2)) return 0;
		return 1;
	}

}