package br.upis.collection;

import java.util.ArrayList;

import java.util.Collections;

import java.util.List;

import br.upis.DataNG;

public class TestList {
	public static void main (String [] args) {
		
		DataNG b = new DataNG ();
		DataNG a = new DataNG ();
		DataNG c = new DataNG ();
		
		List < DataNG > datas;
		datas = new ArrayList < DataNG > ();
		datas.add (a);
		datas.add (b);
		datas.add (c);
		
		System.out.println (" --------------------");
		for (int i = 0; i < datas.size (); i++) {
		System.out.println ("Posi��o " + i + " =>" + datas.get (i));
		}
		
		Collections.sort (datas, new SortDataNG ());
		
		System.out.println (" --------------------");
		System.out.println ("|                    |");
		System.out.println (" --------------------");
		for (int i = 0; i < datas.size (); i++) {
		System.out.println ("Posi��o " + i + " =>" + datas.get (i));
	    }
		System.out.println (" --------------------");
	}

}
