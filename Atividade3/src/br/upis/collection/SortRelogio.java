package br.upis.collection;

import java.util.Comparator;

import br.upis.Relogio;

public class SortRelogio implements Comparator < Relogio > {
	
	@Override
	public int compare (Relogio o1, Relogio o2) {
		if (o1.verificaMenor(o2)) return -1;
		if (o1.equals(o2)) return 0;
		return 1;
	}

}
