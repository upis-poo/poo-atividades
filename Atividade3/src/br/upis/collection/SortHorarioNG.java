package br.upis.collection;

import java.util.Comparator;

import br.upis.HorarioNG;

public class SortHorarioNG implements Comparator < HorarioNG > {
	
	@Override
	public int compare (HorarioNG o1, HorarioNG o2) {
		if (o1.verificaMenor(o2)) return -1;
		if (o1.equals(o2)) return 0;
		return 1;
	}

}