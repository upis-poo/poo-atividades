package br.upis;

public interface IData {

	byte getDia();

	void setDia(byte dia);

	byte getMes();

	void setMes(byte mes);

	short getAno();

	void setAno(short ano);

	String toString();

	void incrementaAno();

	void incrementaAnonvezes(int n);

	void incrementaMes();

	void incrementaMesnvezes(int n);

	void incrementaDia();

	void incrementaDianvezes(int n);

}