package br.upis;

public class Horario implements IHorario {
	private byte hora;
	private byte minuto;
	private byte segundo;
	
	public Horario () {
		setHora ((byte)0);
		setMinuto ((byte)0);
		setSegundo ((byte)0);
	}
	public Horario (byte hora, byte minuto, byte segundo) {
		setHora (hora);
		setMinuto (minuto);
		setSegundo (segundo);
	}
	public Horario (int hora, int minuto, int segundo) {
		this ((byte)hora, (byte)minuto, (byte)segundo);
	}
	public Horario (IHorario horario) {
		this (horario.getHora(), horario.getMinuto(), horario.getSegundo());
	}
	@Override
	public void setHora (byte hora) {
		if (hora >= 0 && hora <= 23) {
			this.hora = hora;
		}
	}
	@Override
	public int getHora () {
		return this.hora;
	}
	@Override
	public void setMinuto (byte minuto) {
		if (minuto >= 0 && minuto <= 59) {
			this.minuto = minuto;
		}
	}
	@Override
	public int getMinuto () {
		return this.minuto;
	}
	@Override
	public void setSegundo (byte segundo) {
		if (segundo >= 0 && segundo <= 59) {
			this.segundo = segundo;
		}
	}
	@Override
	public int getSegundo () {
		return this.segundo;
	}
	@Override
	public String toString () {
		return " (" + getHora () + ":" + getMinuto () + ":" + getSegundo () + ")";
	}
	@Override
	public void incrementaSegundo () {
		byte s = (byte)(segundo + 1);
		if (s == 60) {
			segundo = 0;
			incrementaMinuto ();
		}else {
			segundo = s;
		}
	}
	@Override
	public void incrementaSegundonvezes (int n) {
		int i = 0;
		
		while (n > 0 && i < n) {
			this.incrementaSegundo();
			i++;
		}
	}
	@Override
	public void incrementaMinuto () {
		byte m = (byte)(minuto + 1);
		if (m == 60) {
			minuto = 0;
			incrementaHora ();
		}else {
			minuto = m;
		}
	}
	@Override
	public void incrementaMinutonvezes (int n) {
		int i = 0;
		
		while (n > 0 && i < n) {
			this.incrementaMinuto();
			i++;
		}
	}
	@Override
	public void incrementaHora () {
		byte h = (byte)(hora + 1);
		if (h == 24) {
			hora = 0;
		}else {
			hora = h;
		}
	}
	@Override
	public void incrementaHoranvezes (int n) {
		int i = 0;
		
		while (n > 0 && i < n) {
			this.incrementaHora();
			i++;
		}
	}
	@Override
	public boolean ehUltimoHorario () {
		return hora == 23 && minuto == 59 && segundo == 59;
	}
	@Override
	public boolean ehPrimeiroHorario () {
		return hora == 0 && minuto == 0 && segundo == 0;
	}
	public int hashcode () {
		return hora;
	}
	private boolean equalsHora (Horario hr) {
		return this.getHora() == hr.getHora();
	}
	private boolean equalsMinuto (Horario hr) {
		return this.getMinuto() == hr.getMinuto();
	}
	private boolean equalsSegundo (Horario hr) {
		return this.getSegundo() == hr.getSegundo();
	}
	public boolean equals (Object obj) {
		
		if(this == obj)
			return true;
		
		if(obj == null || obj.getClass()!= this.getClass())
			return false;
		
		Horario hr = (Horario) obj;
		
		return equalsHora (hr) && equalsMinuto (hr) && equalsSegundo (hr);
	}
	@Override
	public int hashCode () {
		final int prime = 31;
		int result = 1;
		result = prime * result + hora;
		result = prime * result + minuto;
		result = prime * result + segundo;
		return result;
	}
	public boolean verificaMenor (Horario hr) {
		
		if (this.getHora() < hr.getHora()) {
			return true;
		}
		if (equalsHora (hr) && this.getMinuto() < hr.getMinuto()) {
			return true;
	    }
		if (equalsHora (hr) && equalsMinuto (hr) && this.getSegundo() < hr.getSegundo()) {
			return true;
		}
		return false;
	}
	public boolean verificaMaior (Horario hr) {
		
		if (this.getHora() > hr.getHora()) {
			return true;
		}
		if (equalsHora (hr) && this.getMinuto() > hr.getMinuto()) {
			return true;
	    }
		if (equalsHora (hr) && equalsMinuto (hr) && this.getSegundo() > hr.getSegundo()) {
			return true;
		}
		return false;
	}
	public boolean verificaLe (Horario hr) {
		
		return this.equals (hr) || this.verificaMenor(hr);
	}
	public boolean verificaGe (Horario hr) {
		
		return this.equals (hr) || this.verificaMaior(hr);
	}
	public int hashCode (Horario hr) {
		int valor;
		valor = hr.segundo + (hr.minuto * 60) + (hr.hora * 3600);
		return valor;
	}
}
